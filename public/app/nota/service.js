import { handleStatus } from "../utils/promise-helpers.js";
import { partialize, pipe } from "../utils/operators.js";
import { Maybe } from "../utils/maybe.js";

import "../utils/array-helpers.js";

const API = "http://localhost:3000/notas";

const getItemsFromNotes = (notasMonada) =>
  notasMonada.map((notas) => notas.$flatMap((nota) => nota.itens));

const filterItemsByCode = (code, itemsMonada) =>
  itemsMonada.map((items) => items.filter((item) => item.codigo === code));

const sumItemsValue = (itemsMonada) =>
  itemsMonada.map((items) =>
    items.reduce((total, item) => total + item.valor, 0)
  );

export const notasService = {
  async listAll() {
    return fetch(API)
      .then(handleStatus)
      .then((notas) => Maybe.of(notas))
      .catch((err) => {
        console.log(err);
        return Promise.reject("Não foi possível obter as notas fiscais");
      });
  },

  async sumItems(code) {
    const filterItems = partialize(filterItemsByCode, code);

    const sumItems = pipe(getItemsFromNotes, filterItems, sumItemsValue);

    return this.listAll()
      .then(sumItems)
      .then((result) => result.getOrElse(0));
  }
};
